FROM golang:1.16-alpine as builder

WORKDIR /app

COPY . .

RUN go mod download
RUN go build -o /tauhq-block-grabber-arko

EXPOSE 10000


CMD [ "/tauhq-block-grabber-arko" ]

