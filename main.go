package main

import (
	"bytes"
	"context"
	"database/sql"
	"encoding/base64"
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"log"

	"net/http"
	"os"
	"strconv"
	"strings"
	"time"

	"github.com/go-redis/redis"
	"github.com/joho/godotenv"
	_ "github.com/lib/pq"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/credentials"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/s3"

	//"golang.org/x/net/websocket"
	"flag"
)

type Block1 struct {
	Hash string
	//Number int
	Number    IntegerWrapper `json:"number,omitempty"`
	Subblocks []Subblock
}

type WSBlock struct {
	Event string
	Data  Block
}

type Block struct {
	Hash string
	//Number       IntegerWrapper `json:"number,omitempty"`
	Number       string
	HLCTimestamp string `json:"hlc_timestamp,omitempty"`
	Previous     string
	Processed    Processed
	//Proofs       []Proof
	//Rewards      []Reward
	//Origin       Origin
	Proofs  json.RawMessage  `json:"proofs"`
	Rewards *json.RawMessage `json:"rewards"`
	Origin  json.RawMessage  `json:"origin"`
	Minted  json.RawMessage  `json:"minted"`
	Repair  bool             `json:"repair"`
}

type LatestBlock struct {
	Number string
}

type Origin struct {
	Signature string
	Sender    string
}

type Reward struct {
	Address string       `json:"key"`
	Balance BalanceValue `json:"value"`
	Reward  string       `json:"reward"`
}

type BalanceValue struct {
	Amount string `json:"__fixed__"`
}

type Proof struct {
	Signature string
	Signer    string
}

type Processed struct {
	Hash        string
	Result      string
	StampsUsed  IntegerWrapper `json:"stamps_used,omitempty"`
	States      StateWrapper   `json:"state,omitempty"`
	Status      IntegerWrapper `json:"status,omitempty"`
	Transaction Transaction
}

type Transaction struct {
	Metadata Metadata
	Payload  Payload
}

type Metadata struct {
	Signature string
}

type Number struct {
	Value int `json:"__fixed__,string"`
}

type IntegerWrapper struct {
	Number
}

//var blockServiceNodeNumber = 1

var blockServiceNodeNumber = 3
var latestBlockNumber = '0'

func test(blockHash string) {
	time.Sleep(1000 * time.Millisecond)

	var apiURL = fmt.Sprintf("https://arko-bs-%d.lamden.io/blocks/%s", blockServiceNodeNumber, blockHash)
	//var apiURL = fmt.Sprintf("https://testnet-v2-bs-lon.lamden.io/blocks/%s", blockHash)

	fmt.Printf("\n\ntest() apiurl: %s\n", apiURL)
	res, err := http.Get(apiURL)
	if err != nil {
		log.Fatal(err)
	}
	body, err := ioutil.ReadAll(res.Body)
	res.Body.Close()
	if err != nil {
		log.Fatal(err)
	}

	fmt.Printf("*************line 126 %+v\n", res)

	if res.StatusCode != 200 {
		fmt.Printf("status code is not 200:: %d\n", res.StatusCode)
		log.Fatal(err)
	}

	var block Block
	err = json.Unmarshal(body, &block)
	if err != nil {
		fmt.Printf("line 119\n")
		log.Fatal(err)
	}

	//hashList = append(hashList, block.Hash)
	//prepend hashList, oldest item comes first

	sqlStatement := `SELECT id FROM blocks WHERE hash = $1`
	rows := db.QueryRow(sqlStatement, blockHash)
	var blockHashFound int
	err = rows.Scan(&blockHashFound)
	if err != nil {
		//log.Fatal(err)
		blockHashFound = 0
		hashList = append([]string{blockHash}, hashList...)
	}

	if blockHashFound == 0 {
		fmt.Printf("blockHash not found %s %s\n", block.HLCTimestamp, blockHash)
		/*
			first block always has number: 0
				{
				"hash": "2bb4e112aca11805538842bd993470f18f337797ec3f2f6ab02c47385caf088e",
				"number": "0",
				"hlc_timestamp": "0000-00-00T00:00:00.000000000Z_0",
				"previous": "0000000000000000000000000000000000000000000000000000000000000000",
				"origin": {
				"signature": "7b2b8fa3205418d94b18eab358722948ba1b1d85daba523959516eaad9e729a209f7424085786febe684a6216733df5e4801ea26edb2d312aa036e027342bc0d",
				"sender": "0000803efd5df09c75c0c6670742db5074e5a011b829dfd8a0c50726d263a345"
				}
				}
		*/

		fmt.Printf("Calling test() for previous block hash %s current block hlc: %s\n", block.Previous, block.HLCTimestamp)

		if block.Previous != "0000000000000000000000000000000000000000000000000000000000000000" {
			/*
				blockServiceNodeNumber++
				if blockServiceNodeNumber > 3 {
					blockServiceNodeNumber = 1
				}
			*/

			test(block.Previous)
		}
	} else {
		fmt.Printf("blockHash found %s %s\n", block.HLCTimestamp, block.Hash)
	}

}

func (w *IntegerWrapper) UnmarshalJSON(data []byte) error {
	if id, err := strconv.Atoi(string(data)); err == nil {
		w.Value = id
		return nil
	}
	return json.Unmarshal(data, &w.Number)
}

type Subblock struct {
	//InputHash    string `json:"input_hash"`
	//Subblock     int
	Subblock     IntegerWrapper `json:"subblock,omitempty"`
	Transactions []Transaction
	//MerkleLeaves []string `json:"merkle_leaves"`
}

type Transaction1 struct {
	Hash   string
	Result string
	//StampsUsed int `json:"stamps_used"`
	StampsUsed IntegerWrapper `json:"stamps_used,omitempty"`
	//State          json.RawMessage `json:"state"`
	//States []State `json:"state,omitempty"`
	States StateWrapper `json:"state,omitempty"`
	//Status         int
	Status         IntegerWrapper `json:"status,omitempty"`
	Subtransaction Subtransaction `json:"transaction"`
}

type State struct {
	Key   string
	Value json.RawMessage `json:"value"`
}

type StateWrapper struct {
	States []State `json:"state"`
}

func (w *StateWrapper) UnmarshalJSON(data []byte) error {
	if err := json.Unmarshal(data, &w.States); err != nil {
		return nil
	}
	return json.Unmarshal(data, &w.States)
}

type Subtransaction struct {
	Metadata Metadata
	Payload  Payload
}

type Metadata1 struct {
	Signature string
	//Timestamp int
	Timestamp IntegerWrapper `json:"timestamp,omitempty"`
}

type Payload struct {
	Contract string
	Function string
	Kwargs   json.RawMessage `json:"kwargs"`
	//Nonce          int
	Nonce     IntegerWrapper `json:"nonce,omitempty"`
	Processor string
	Sender    string
	//StampsSupplied int             `json:"stamps_supplied"`
	StampsSupplied IntegerWrapper `json:"stamps_supplied,omitempty"`
}

type ContractKwargs struct {
	Code string
	Name string
}

type KwargsKV struct {
	Key   string
	Value string
}

type StateByteCode struct {
	ByteCode string `json:"__bytes__"`
}

type StateDB struct {
	Key   string
	Value json.RawMessage
}

/*
type LatestBlock struct {
	LatestBlockNumber int `json:"latest_block_number"`
}
*/

type KwargsSell struct {
	Contract string   `json:"contract"`
	Amount   KWAmount `json:"token_amount"`
}

type KwargsBuy struct {
	Contract string   `json:"contract"`
	Amount   KWAmount `json:"currency_amount"`
}

type KWAmount struct {
	Fixed string `json:"__fixed__"`
}

type TAUTicker struct {
	//PriceUSD string `json:"price_usd"`
	Value string `json:"value"`
}

var syncRunning int = 0

var db *sql.DB
var redisClient *redis.Client
var tauPiceLastUpdate = 0
var tauPriceUSD = ""
var hashList []string

func NewNullString(s string) sql.NullString {
	if len(s) == 0 {
		return sql.NullString{}
	}
	return sql.NullString{
		String: s,
		Valid:  true,
	}
}

func checkTAUPrice() {
	//fmt.Printf("Checking TAU Price...")
	now := int(time.Now().Unix())
	if now-tauPiceLastUpdate > 3600 || tauPriceUSD == "" {
		res, err := http.Get("https://api.coinpaprika.com/v1/ticker/tau-lamden")
		//res, err := http.Get("https://rocketswap.exchange/api/tau_last_price")
		if err != nil {
			log.Fatal(err)
		}
		body, err := ioutil.ReadAll(res.Body)
		res.Body.Close()
		if err != nil {
			log.Fatal(err)
		}
		if res.StatusCode != 200 {
			log.Fatal("line 325: Unexpected status code", res.StatusCode)
		}
		var tauTicker TAUTicker
		err = json.Unmarshal(body, &tauTicker)
		if err != nil {
			log.Fatal(err)
		}
		tauPriceUSD = tauTicker.Value
		tauPiceLastUpdate = now
		//fmt.Printf("updated TAU price\n")
	} else {
		//fmt.Printf("no update needed\n")
	}

	t := time.Now().UTC()
	yyyymmdd := t.Format("20060102")
	err := redisClient.Do("HSET", "h_tau_daily_usd_price:"+yyyymmdd, "price", tauPriceUSD).Err()
	if err != nil {
		//sentry.CaptureException(err)
		fmt.Printf("line 334\n")
		log.Fatal(err)
	}
	fmt.Printf("tauPriceUSD %s\n", tauPriceUSD)

}

func putObject(filename string) {

	bucket := aws.String(os.Getenv("B2BUCKET"))
	key := aws.String("img/token_logo/" + filename + ".jpg")

	file, err := os.Open("/tmp/tmp.jpg")
	if err != nil {
		fmt.Printf("cannot open /tmp/tmp/jpg\n")
		log.Fatal(err)
	}
	defer file.Close()
	fileInfo, _ := file.Stat()
	size := fileInfo.Size()
	buffer := make([]byte, size) // read file content to buffer

	file.Read(buffer)
	fileBytes := bytes.NewReader(buffer)

	s3Config := &aws.Config{
		Credentials:      credentials.NewStaticCredentials(os.Getenv("B2KEYID"), os.Getenv("B2APPID"), ""),
		Endpoint:         aws.String("https://s3." + os.Getenv("B2REGION") + ".backblazeb2.com"),
		Region:           aws.String(os.Getenv("B2REGION")),
		S3ForcePathStyle: aws.Bool(true),
	}
	newSession := session.New(s3Config)
	s3Client := s3.New(newSession)

	fmt.Printf("filename: %s key %s\n", filename, key)
	_, err = s3Client.PutObject(&s3.PutObjectInput{
		Body:   fileBytes,
		Bucket: bucket,
		Key:    key,
	})

	if err != nil {
		fmt.Printf("Failed to upload object %s/%s, %s\n", *bucket, *key, err.Error())
		log.Fatal(err)
		return
	}
	fmt.Printf("Successfully uploaded key %s\n", *key)
}

func processBlock(block Block) {
	//func processBlock(blockHash string) {
	ctx, _ := context.WithCancel(context.Background())
	tx, err := db.BeginTx(ctx, nil)
	if err != nil {
		log.Fatal(err)
	}
	/*


		var apiURL = fmt.Sprintf("https://arko-bs-%d.lamden.io/blocks/%s", blockServiceNodeNumber, blockHash)

		fmt.Printf("apiurl: %s\n", apiURL)
		res, err := http.Get(apiURL)
		if err != nil {
			log.Fatal(err)
		}
		body, err := ioutil.ReadAll(res.Body)
		res.Body.Close()
		if err != nil {
			log.Fatal(err)
		}
		if res.StatusCode != 200 {
			log.Fatal("line 410: Unexpected status code: ", res.StatusCode)
		}

		var block Block
		err = json.Unmarshal(body, &block)
		if err != nil {
			fmt.Printf("line 119\n")
			log.Fatal(err)
		}
	*/

	var rewards []Reward
	var txValueUSD string
	var tokenName string
	var tauSwapRate string
	var txAmount float64
	var addressAmountString string
	var addressesString string

	var epoch int64
	ts := strings.Replace(block.HLCTimestamp, "_0", "", -1)
	//fmt.Printf("%s\n", ts)

	if ts != "0000-00-00T00:00:00.000000000Z" {
		thetime, e := time.Parse(time.RFC3339, ts)
		if e != nil {
			panic("Can't parse time format")
		}
		epoch = thetime.Unix()
	} else {
		epoch = 0
	}

	/*
		The sender address might show up more than once in multiple states, we only consider the following as one tx for the same sender
		con_rswp_lst001.balances:fcefe7743fa70c97ae2d5290fd673070da4b0293da095f0ae8aceccf5e62b6a1
		currency.balances:fcefe7743fa70c97ae2d5290fd673070da4b0293da095f0ae8aceccf5e62b6a1
	*/

	addressMap := make(map[string]string)

	//insert below

	blockID := 0
	sqlStatement := `
	INSERT INTO blocks (hash, timestamp, txn, previous_hash, proofs, origin, rewards, number, repair)
	VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9)
	RETURNING id`

	var txCount int = 0
	if block.Number == "0" {
		txCount = 0
	} else {
		_ = json.Unmarshal(*block.Rewards, &rewards)

		for i := 0; i < len(rewards); i++ {
			rewardAddress := strings.Split(rewards[i].Address, ":")[1]
			//fmt.Printf("insert block rewards for address: %s balance: %s reward: %s\n", rewardAddress, rewards[i].Balance.Amount, rewards[i].Reward)
			sqlStatement := `
		INSERT INTO top_addresses (address, balance)
		VALUES ($1, $2) ON CONFLICT(address) DO UPDATE SET balance = $2 RETURNING id`
			id := 0
			err := db.QueryRow(sqlStatement, rewardAddress, rewards[i].Balance.Amount).Scan(&id)
			if err != nil {
				log.Fatal(err)
			}
		}
		txCount = 1
		err = db.QueryRow(sqlStatement, block.Hash, epoch, txCount, block.Previous, block.Proofs, block.Origin, block.Rewards, block.Number, block.Repair).Scan(&blockID)
		if err != nil {
			fmt.Printf("line 731 cannot insert blocks %s epoch %d\n", block.Hash, epoch)
			panic(err)
		}
		err = redisClient.Set("latest_block_id", blockID, 0).Err()
		if err != nil {
			fmt.Println(err)
		}

		//get token amount
		if block.Processed.Transaction.Payload.Contract == "con_rocketswap_official_v1_1" && (block.Processed.Transaction.Payload.Function == "buy" || block.Processed.Transaction.Payload.Function == "sell") {
			if block.Processed.Transaction.Payload.Function == "buy" {
				var kwargsBuy KwargsBuy
				err = json.Unmarshal(block.Processed.Transaction.Payload.Kwargs, &kwargsBuy)
				txAmountTmp, err := strconv.ParseFloat(kwargsBuy.Amount.Fixed, 32)
				if err != nil {
					//log.Fatal(err)
				}
				tokenName = kwargsBuy.Contract
				txAmount = txAmountTmp
			} else {
				var kwargsSell KwargsSell
				err = json.Unmarshal(block.Processed.Transaction.Payload.Kwargs, &kwargsSell)
				txAmountTmp, err := strconv.ParseFloat(kwargsSell.Amount.Fixed, 32)
				if err != nil {
				}
				tokenName = kwargsSell.Contract
				txAmount = txAmountTmp
			}
		}
		var addressList []string

		for _, state := range block.Processed.States.States {
			keySplit := strings.Split(state.Key, ":")
			/*
				don't store unwanted value in the 2nd column eg: win30, names
				con_hopium.games:win30:playercount
				con_hopium.gameNames:names
			*/

			//get Swap Rate
			if state.Key == "con_rocketswap_official_v1_1.prices:"+tokenName {
				byteValue, err := json.Marshal(state.Value)
				amountString := string(byteValue)
				if err != nil {
					log.Fatal(err)
				}
				var balance string
				if strings.Contains(amountString, "fixed") {
					balanceSplit := strings.Split(amountString, "\"")
					balance = balanceSplit[3]
				} else {
					balance = amountString
				}
				err = redisClient.Do("HMSET", "h_tau_token:"+tokenName, "last_price_tau", balance).Err()
				if err != nil {
					//sentry.CaptureException(err)
				}
				tauSwapRate = balance
			}

			/*
				update currency balances and token balances:
				"key": "currency.balances:7cc994343843d5d00f7cf7e26420744ef50e60cd732d24bd0c6f5aa9cf438c13",
				"key": "con_jeff_lst001.balances:ec3634c3e2b9dadaa58568cfbbadcfab84d5b75192cfa06a85f40b41a9b19f1d",

				note that we ignore arrays with more than 2 parts:
				"key": "currency.balances:e12725907c45a3736d4eb0224e922383e4c4285a71c92fe88536330aee9ff412:con_multisend",
			*/

			if len(keySplit) == 2 {

				keyNameSplit := strings.Split(keySplit[0], ".")
				if len(keyNameSplit) == 2 {
					if keyNameSplit[1] == "balances" {
						address := keySplit[1]
						byteValue, err := json.Marshal(state.Value)
						amountString := string(byteValue)
						if err != nil {
							log.Fatal(err)
						}
						var balance string
						if strings.Contains(amountString, "fixed") {
							balanceSplit := strings.Split(amountString, "\"")
							balance = balanceSplit[3]
						} else {
							balance = amountString
						}

						/*
							In https://masternode-01.lamden.io/blocks?num=1015451,
							there's a line "key": "con_dontbuyit.balances:" and it has no address. We want to get rid of 'ghost address'
						*/
						if len(address) > 0 {
							addressMap[address] = "0"
						}
						addressFound := 0
						for _, n := range addressList {
							if n == address {
								addressFound = 1
								break
							}
						}
						var txnIncrement int
						if addressFound == 0 {
							txnIncrement = 1
							addressList = append(addressList, address)
						} else {
							txnIncrement = 0
						}

						if keyNameSplit[0] == "currency" {

							//update currency balance
							sqlStatement := `
									INSERT INTO top_addresses (address, balance, txn, first_tx_timestamp)
									VALUES ($1, $2, 1, $3) ON CONFLICT(address) DO UPDATE SET balance = $2, txn = top_addresses.txn + $4
									RETURNING id`
							id := 0
							err = db.QueryRow(sqlStatement, address, balance, epoch, txnIncrement).Scan(&id)
							if err != nil {
								log.Fatal(err)
							}
						} else {
							tokenName := keyNameSplit[0]
							if block.Processed.Transaction.Payload.Contract == "submission" && block.Processed.Transaction.Payload.Function == "submit_contract" && block.Processed.Status.Value == 0 {
								var contractKwargs ContractKwargs
								err = json.Unmarshal(block.Processed.Transaction.Payload.Kwargs, &contractKwargs)
								contractName := contractKwargs.Name
								fmt.Printf("line 335 test: %s\n", block.Processed.Transaction.Payload.Kwargs)
								//convert rawMessage to string
								jKwargs, err := json.Marshal(&block.Processed.Transaction.Payload.Kwargs)
								if err != nil {

									panic(err)
								}
								kwargsStr := string(jKwargs)
								tokenNameIndex := strings.Index(kwargsStr, "token_name")
								tokenSymbolIndex := strings.Index(kwargsStr, "token_symbol")
								if tokenNameIndex > -1 && tokenSymbolIndex > -1 {
									err = redisClient.Do("HMSET", "h_tau_token:"+contractName, "last_price_tau", "0").Err()
									if err != nil {
										fmt.Printf("line 348 error: contractName: %s\n", contractName)
										panic(err)
										//sentry.CaptureException(err)
									}
								}
							}
							_, err := redisClient.HGet("h_tau_token:"+tokenName, "name").Result()
							if err != nil {
								/*
									only increment the txn for key like
									con_blacktau.balances:d3b192f8d65375e1c5e80c306a99c3ae7979a98f90c92776db7914cc1b3ec181
									con_pixel_whale_master_v1.balances:d9af7cb1fff35a6a41a5d3672e2f3cc48de4d588796ff97b0191ce1a12258b4c
									it doesn't update token_balance because the key has nothing to do with token
								*/
								sqlStatement := `
										INSERT INTO top_addresses (address, txn, first_tx_timestamp)
										VALUES ($1, 1, $2) ON CONFLICT(address) DO UPDATE SET txn = top_addresses.txn + $3 RETURNING id`
								id := 0
								err = db.QueryRow(sqlStatement, address, epoch, txnIncrement).Scan(&id)
								if err != nil {
									log.Fatal(err)
								}
							} else {
								//https://stackoverflow.com/questions/32662478/golang-sql-package-jsonb-operator
								sqlStatement := `
										INSERT INTO top_addresses (address, token_balance, txn, first_tx_timestamp)
										VALUES ($3, json_build_object($1::varchar, $2::float)::jsonb, 1, $4) ON CONFLICT(address) DO UPDATE SET token_balance = top_addresses.token_balance || json_build_object($1::varchar, $2::float)::jsonb, txn = top_addresses.txn + $5 RETURNING id`
								id := 0
								err = db.QueryRow(sqlStatement, tokenName, balance, address, epoch, txnIncrement).Scan(&id)
								if err != nil {
									log.Fatal(err)
								}
								/*
									a sender can have more than one record in a transaction eg one for currency, another for token:
									https://masternode-01.lamden.io/blocks?num=61511
									currency.balances:0e265d958966f0aea59b3bf2d310670d3afdc4ad04ee7cb40ba96ae6666aaf4b
									con_moon_landing.balances:0e265d958966f0aea59b3bf2d310670d3afdc4ad04ee7cb40ba96ae6666aaf4b
									So we have two different trackers senderCurrencyStateKeyCount and senderTokenStateKeyCount
								*/
								//to be fixed: is not captured
							}
						}

					}
				}
			}
		}

		if block.Processed.Transaction.Payload.Contract == "con_rocketswap_official_v1_1" && (block.Processed.Transaction.Payload.Function == "buy" || block.Processed.Transaction.Payload.Function == "sell") {
			tauPriceUSDFloat, err := strconv.ParseFloat(tauPriceUSD, 64)
			if err != nil {
				//log.Fatal(err)
			}
			if block.Processed.Transaction.Payload.Function == "sell" {
				tauSwapRateFloat, err := strconv.ParseFloat(tauSwapRate, 64)
				if err != nil {
					//log.Fatal(err)
				}
				txValueUSD = fmt.Sprintf("%f", float64(txAmount)*tauSwapRateFloat*float64(tauPriceUSDFloat))
			} else {
				txValueUSD = fmt.Sprintf("%f", float64(txAmount)*float64(tauPriceUSDFloat))
			}
		}

		id := 0
		var hasError bool = false
		if block.Processed.Status.Value == 1 {
			hasError = true
		}
		var kwargsAmount string

		if (block.Processed.Transaction.Payload.Contract == "con_token_swap" && block.Processed.Transaction.Payload.Function == "disperse") || (block.Processed.Transaction.Payload.Contract == "currency" && block.Processed.Transaction.Payload.Function == "transfer") {
			byteValue, err := json.Marshal(block.Processed.Transaction.Payload.Kwargs)
			amountString := string(byteValue)
			amountString = strings.Replace(amountString, "{", "", -1)
			amountString = strings.Replace(amountString, "}", "", -1)
			amountString = strings.Replace(amountString, "\"", "", -1)
			var balanceArray = strings.Split(amountString, ",")
			for _, element := range balanceArray {
				if strings.Contains(element, "fixed") {
					kwargsAmount = strings.Split(element, ":")[2]
				} else {
					if strings.Contains(element, "amount") {
						kwargsAmount = strings.Split(element, ":")[1]
					}
				}
			}
			if err != nil {
				log.Fatal(err)
			}
		}

		for key, _ := range addressMap {
			if !strings.Contains(key, "\"") {

				// sanitize key with \n
				// eg: "key": "con_nebula.balances:00bb379157172a1e8a764e5a29adeb8d6076c580fe7c076d79559551db9858b6\n",
				if strings.Contains(key, "\n") {
					fixed := strings.TrimSuffix(key, "\n")
					addressAmountString += "\"" + fixed + "\": \"0\","
					addressesString += fixed + ","
				} else {
					addressAmountString += "\"" + key + "\": \"0\","
					addressesString += key + ","
				}
			}

		}

		addressAmountString = "{" + strings.TrimSuffix(addressAmountString, ",") + "}"
		addressesString = "{" + strings.TrimSuffix(addressesString, ",") + "}"
		byteValue, err := json.Marshal(block.Processed.States.States)
		jsonStates := string(byteValue)

		//fmt.Printf("Storing!!!。 TX %s at block %s jsonstring: %s\n", block.Processed.Hash, block.Number, addressAmountString)

		if block.Number != "0" {

			var kwargs = block.Processed.Transaction.Payload.Kwargs
			if len(block.Processed.Transaction.Payload.Kwargs) == 0 {
				fmt.Printf("kwargs not found\n")
				kwargs = json.RawMessage([]byte("[]"))
			}
			sqlStatement = `
	INSERT INTO transactions (signature, hash, sender, processor, timestamp, block_id, nonce, stamps_used, stamps_supplied, has_error, result, contract, function, kwargs, state, address_balance, kwargs_amount, tau_price_usd, tau_swap_rate, tx_value_usd, addresses)
	VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12, $13, $14, $15, $16, $17, $18, $19, $20, $21)
	RETURNING id`
			err = db.QueryRow(sqlStatement,
				block.Processed.Transaction.Metadata.Signature,
				block.Processed.Hash,
				block.Processed.Transaction.Payload.Sender,
				block.Processed.Transaction.Payload.Processor,
				epoch,
				blockID,
				block.Processed.Transaction.Payload.Nonce.Value,
				block.Processed.StampsUsed.Value,
				block.Processed.Transaction.Payload.StampsSupplied.Value,
				hasError,
				block.Processed.Result,
				block.Processed.Transaction.Payload.Contract,
				block.Processed.Transaction.Payload.Function,
				kwargs,
				NewNullString(jsonStates), addressAmountString, NewNullString(kwargsAmount), tauPriceUSD, tauSwapRate, txValueUSD, addressesString).Scan(&id)

			if err != nil {
				/*
					removing index transactions_address_balance_indx, otherwise it will complain index row size 2792 exceeds maximum 2712 for index "transactions_address_balance_indx"
					see: https://dba.stackexchange.com/questions/25138/index-max-row-size-error
						Storing TX 1f71e197204a6f4e83df2527e0d4fd71f9ad1cd1350d9d6a34f698a15685430f at block 37011 jsonstring: {"b00e36d00873ded7569eac854f36eb5f7b3e5684d0e74ea27ecadea617354edd": "0"}
						processing tx: 1bfabb90629f052f34c57b9aa0ce268113969867464632b3cfed56f9af310653
						Storing TX 1bfabb90629f052f34c57b9aa0ce268113969867464632b3cfed56f9af310653 at block 37011 jsonstring: {"45389da6e7f0ab1dcbdac46b15a5454519649423f0f19e56b3c5f336c63bcb9a": "0","4ba35a3ccde7cbb5fd9f592e04ab82176cab7aa22a7447a410b336803c10603d": "0","ae7d14d6d9b8443f881ba6244727b69b681010e782d4fe482dbfb0b6aca02d5d": "0","822b7fb32a1d93f032d183e5ecd57d1b6db997d9ba5ff372b113d529f84ab689": "0","68a7c033f148919d018bd0a1876c22b460401a1778aba84dc41ef52b8bf73650": "0","fa175c739dfd5e0b78025d83eadd84d8abeb2bba980486fa2c4ca23178fbcc48": "0","d5fd3d6daf2c7291fc9b0b002d7c66d8a491e2e5398cd1263963ffd8477c13f3": "0","7ce3ebbeb1f48f13152af41672d8ba0967c04ecf921ab84233d9cfdaf638ba1a": "0","288c6365ec250aa21dab1c51f6e46b0f54bacf94cf086edcf4dad71cdc5a1e85": "0","fca75d5cad58c1c3f5795b4e21faa3ceadb5426a1ce5ba65d35c47a61785795c": "0","e12725907c45a3736d4eb0224e922383e4c4285a71c92fe88536330aee9ff412": "0","959890c06c6f82d746890dd7c6e68fa688557d922504c7a4620436b8ca27616f": "0","2f0f512cb8f6f9f2323a50dad78e1f087c391f98908c19bd8b26cd9423f78b84": "0","ceb5df2091a62284b8ce251a77c1fadb8951ab8c69ae641d50c9d12b14781e3f": "0","8fd388642cee1e5ad41300713056b6a8e5188eca29f3001e646434c39151796e": "0","e74a4df29f5d1f6dbabf830dd355d1328908e55357a90854b69f3b0ea973f35b": "0","61d8ace5bc83d82f1352a101122808aa8642176d57b89ca628f11dd6d2da4c7b": "0","b51d0794c83daa17f48c86e7e86b4c8ae177fa95409318f2e7f925b79373a147": "0","83626b9a67fe1a07272d7e739919caf296390a09f3097f4f94140842166c7173": "0","375e9513c2d641032d740aab00f74149245417bad327b08aa2785ff845819b7a": "0","b53d865d448b1a2cdda2cad2733f97843747264d4294a9b1152ac075b2f10409": "0","be2d9fd1d8c8e358166a326cd87ea8d37a6a09ab0ae5b45fbdb0b2e3340dffe3": "0","451dc0f15727a5ed1577580c937986a4ec91442e094d83b636ccf5400d5b4ba6": "0","ae9d55ed949e382c417db4b24752f4447b6d42f5a2534901fac3af3985a10e70": "0","24be464c1ff2a90c1b0c3604c95db949f53433ac3dff9db7ff61016d792a77f8": "0","7663493efd0badcb4922500e0780b48ca7be86c823821fb940c4a90fec4c75b4": "0","8918fdcb79e7d87730c764bdbfbce2a1b6fbb969ffcc84d7289b0e196db2d629": "0","732843ec70f38705b1a0affd58bfb0ffe186f71db255c28f39bc277e8810e3b3": "0","49aceeabdccdcb39f8c2c112e110ead1a5fef22c644825c1917b2df3204c433f": "0","24f4184c9d9e8e8440067e75fb4c82d44c51c529581dd40e486a0ca989639600": "0","8a4be14635c1383ae05a4d3e0e56893bd6e54fee99f12a8a76ff1d5746d5281d": "0","08ee49b6ba05289bc54251a1abe6156e74e820b7249920ad1c8bc698dc0b6f44": "0","8ece73c57b89a1c5a296f1cbbcc057715901b66c8bf416688fe9ec12ec1db9bc": "0","b100171498a7040e56f6a8205eeb514f73d9da966f44586ec18560045268db8c": "0","b00e36d00873ded7569eac854f36eb5f7b3e5684d0e74ea27ecadea617354edd": "0","a27297ef54d67c72ebe8797d16e19e86ee78fae63af20e3da30ea7bba09a102b": "0","c369d77b6de2900e1f8064744bb45282f5e3eaa2def30b7c61e850dc188d98b7": "0","9ac9c92d84d2c036ccc5db9a55acc02438f96297837851fecae34e779a28c8c7": "0"}
						309 errorpanic: pq: index row size 2792 exceeds maximum 2712 for index "transactions_address_balance_indx"
				*/
				fmt.Printf("line 503 insert into transactions failed\n")
				panic(err)
			}

			redisTotalBurned, err := redisClient.Get("total_burned").Result()
			if err != nil {
				fmt.Printf("has error line 564: ")
			}
			totalBurned, err := strconv.ParseFloat(redisTotalBurned, 64)
			if err != nil {
				fmt.Println(err)
			}

			totalBurned += float64(block.Processed.StampsUsed.Value) * float64(0.01) / float64(169)
			err = redisClient.Set("total_burned", totalBurned, 0).Err()
			if err != nil {
				fmt.Println(err)
			}

			err = redisClient.Set("latest_transaction_id", id, 0).Err()
			if err != nil {
				fmt.Println(err)
			}

		}

		if block.Processed.Transaction.Payload.Contract == "submission" && block.Processed.Transaction.Payload.Function == "submit_contract" && block.Processed.Status.Value == 0 {
			fmt.Printf("inside submission block\n")
			var contractKwargs ContractKwargs
			err = json.Unmarshal(block.Processed.Transaction.Payload.Kwargs, &contractKwargs)
			contractName := contractKwargs.Name
			sqlStatement := `SELECT id FROM contracts WHERE name = $1`
			rows := db.QueryRow(sqlStatement, contractName)
			var contractIDFound int
			err = rows.Scan(&contractIDFound)
			if err != nil {
				//log.Fatal(err)
				contractIDFound = 0
			}
			if contractIDFound == 0 {
				tokenName := ""
				tokenSymbol := ""
				var totalSupply int64
				fmt.Printf("test: %s\n", block.Processed.Transaction.Payload.Kwargs)
				//convert rawMessage to string
				jKwargs, err := json.Marshal(&block.Processed.Transaction.Payload.Kwargs)
				if err != nil {

					panic(err)
				}
				kwargsStr := string(jKwargs)
				tokenNameIndex := strings.Index(kwargsStr, "token_name")
				tokenSymbolIndex := strings.Index(kwargsStr, "token_symbol")
				if tokenNameIndex > -1 && tokenSymbolIndex > -1 {
					kwargsStrGroup := strings.Split(kwargsStr, "\\n")
					totalSupplyFound := 0
					for i := 0; i < len(kwargsStrGroup); i++ {
						if strings.Contains(kwargsStrGroup[i], "metadata['token_name'] =") || strings.Contains(kwargsStrGroup[i], "metadata['token_symbol'] =") || strings.Contains(kwargsStrGroup[i], "metadata['total_supply'] =") {
							kwargsStrGroup[i] = strings.Replace(kwargsStrGroup[i], "\"", "", -1)
							kwargsStrGroup[i] = strings.Replace(kwargsStrGroup[i], "\\", "", -1)
							lineGroup := strings.Split(kwargsStrGroup[i], "=")
							if strings.Contains(kwargsStrGroup[i], "token_name") {
								tokenName = lineGroup[1]
							} else if strings.Contains(kwargsStrGroup[i], "token_symbol") {
								tokenSymbol = lineGroup[1]
							} else if strings.Contains(kwargsStrGroup[i], "total_supply") {
								/*
									filter unwated lines such as
									balances[ctx.caller] += amount
									if(balances[ctx.caller] < metadata['min_token_for_redistribution']):
								*/
								if totalSupplyFound == 0 {
									lineGroup[1] = strings.Replace(lineGroup[1], "_", "", -1)
									givenTotalSupply, err := strconv.ParseInt(lineGroup[1], 10, 64)
									if err != nil {
										fmt.Printf("line 685: totalSupply is string %s\n", lineGroup[1])
										totalSupply = 0
									} else {
										fmt.Printf("line 688 total supply is integer %d\n", givenTotalSupply)
										totalSupply = givenTotalSupply
										totalSupplyFound = 1
									}
								}
							}
						}
					}

					fmt.Printf("line 580 inserting new contract\n")
					sqlStatement = `INSERT INTO contracts(name, token_name, token_symbol, total_supply, is_token) VALUES($1, $2, $3, $4, '1') RETURNING id`
					id := 0
					tokenName = strings.TrimSpace(tokenName)
					tokenSymbol = strings.TrimSpace(tokenSymbol)
					err = db.QueryRow(sqlStatement, contractName, tokenName, tokenSymbol, totalSupply).Scan(&id)
					if err != nil {
						fmt.Printf("stmt: %s\n", sqlStatement)
						fmt.Printf("line 554: totalSupply: %d\n", totalSupply)
						panic(err)
					}

					if id > 0 {
						err = redisClient.Set("latest_contract_id", id, 0).Err()
						if err != nil {
							fmt.Println(err)
						}
					}

					fmt.Printf("line 597 redis: name: %s symbol: %s totalSupply: %d contractName: %s\n", tokenName, tokenSymbol, totalSupply, contractName)
					err = redisClient.Do("HMSET", "h_tau_token:"+contractName, "name", tokenName, "symbol", tokenSymbol, "total_supply", totalSupply, "last_price_tau", "0").Err()
					if err != nil {
						fmt.Printf("line 600 error: name: %s symbol: %s totalSupply: %d\n", tokenName, tokenSymbol, totalSupply)
						panic(err)
						//sentry.CaptureException(err)
					}

					sqlStatement = `
							SELECT timestamp, sender, hash, state FROM transactions WHERE contract = 'submission' AND function = 'submit_contract' AND has_error = false AND kwargs->>'name' = $1`
					var timestamp int
					var sender string
					var hash string
					var state string
					var byteCode StateByteCode
					err = db.QueryRow(sqlStatement, contractName).Scan(&timestamp, &sender, &hash, &state)
					if err != nil {
						fmt.Printf("line 464 contract_name: %s\n", contractName)
						panic(err)
					}
					if timestamp > 0 {
						var states []StateDB
						//convert state (string) to rawmessage
						err := json.Unmarshal([]byte(state), &states)
						if err != nil {
							log.Fatal(err)
						}
						for _, state := range states {
							// extract bytecode into StateByteCode Struct
							if state.Key == fmt.Sprintf(contractName+".__compiled__") {
								err = json.Unmarshal(state.Value, &byteCode)
							}
						}
					}
					sqlStatement = `UPDATE contracts SET contract_code = $1, contract_creator = $2, transaction_id = $3, contract_created = $4, byte_code = $5 WHERE name = $6 RETURNING id`
					contractID := 0
					err = db.QueryRow(sqlStatement, contractKwargs.Code, sender, hash, timestamp, byteCode.ByteCode, contractName).Scan(&contractID)
					if err != nil {
						log.Fatal(err)
					}

				} else {
					fmt.Printf("IS NOT TOKEN\n")

					fmt.Printf("line 647 inserting new contract\n")
					sqlStatement = `INSERT INTO contracts(name, is_token) VALUES($1, '0') RETURNING id`
					id := 0
					err = db.QueryRow(sqlStatement, contractName).Scan(&id)
					if err != nil {
						fmt.Printf("stmt: %s\n", sqlStatement)
						panic(err)
					}
					if id > 0 {
						err = redisClient.Set("latest_contract_id", id, 0).Err()
						if err != nil {
							fmt.Println(err)
						}
					}
					sqlStatement = `
							SELECT timestamp, sender, hash, state FROM transactions WHERE contract = 'submission' AND function = 'submit_contract' AND has_error = false AND kwargs->>'name' = $1`
					var timestamp int
					var sender string
					var hash string
					var state string
					var byteCode StateByteCode
					err = db.QueryRow(sqlStatement, contractName).Scan(&timestamp, &sender, &hash, &state)
					if err != nil {
						fmt.Printf("line 678 contract_name: %s\n", contractName)
						panic(err)
					}
					if timestamp > 0 {
						var states []StateDB
						//convert state (string) to rawmessage
						err := json.Unmarshal([]byte(state), &states)
						if err != nil {
							log.Fatal(err)
						}
						for _, state := range states {
							// extract bytecode into StateByteCode Struct
							if state.Key == fmt.Sprintf(contractName+".__compiled__") {
								err = json.Unmarshal(state.Value, &byteCode)
							}
						}
					}
					sqlStatement = `UPDATE contracts SET contract_code = $1, contract_creator = $2, transaction_id = $3, contract_created = $4, byte_code = $5 WHERE name = $6 RETURNING id`
					contractID := 0
					err = db.QueryRow(sqlStatement, contractKwargs.Code, sender, hash, timestamp, byteCode.ByteCode, contractName).Scan(&contractID)
					if err != nil {
						fmt.Printf("line 691 failed to update contracts")
						log.Fatal(err)
					}

				}
			} else {
				fmt.Printf("contract found\n")
			}
		}

		if block.Processed.Transaction.Payload.Function == "change_metadata" && block.Processed.Status.Value == 0 {
			var kwargsKV KwargsKV
			err = json.Unmarshal(block.Processed.Transaction.Payload.Kwargs, &kwargsKV)
			if kwargsKV.Key == "token_logo_base64_png" {
				fmt.Printf("updating contract for contract: %s\n", block.Processed.Transaction.Payload.Contract)

				dec, err := base64.StdEncoding.DecodeString(kwargsKV.Value)
				if err != nil {
					fmt.Printf("line 709: error logo")
					//panic(err)
				} else {

					fmt.Printf("dec: %+v\n", dec)

					f, err := os.Create("/tmp/tmp.jpg")
					if err != nil {
						panic(err)
					}
					defer f.Close()
					if _, err := f.Write(dec); err != nil {
						panic(err)
					}
					if err := f.Sync(); err != nil {
						panic(err)
					}

					putObject(block.Processed.Transaction.Payload.Contract)

					sqlStatement = `UPDATE contracts SET token_logo = $2 WHERE name = $1 RETURNING id`
					contractID := 0
					err = db.QueryRow(sqlStatement, block.Processed.Transaction.Payload.Contract, kwargsKV.Value).Scan(&contractID)
					if err != nil {
						panic(err)
					}
				}
			} else if kwargsKV.Key == "token_logo_url" {
				resp, err := http.Get(kwargsKV.Value)
				if err != nil {
					fmt.Printf("line 733: error logo")
					//panic(err)
				} else {
					defer resp.Body.Close()
					out, err := os.Create("/tmp/tmp.jpg")
					if err != nil {
						panic(err)
					}
					defer out.Close()
					// Write the body to file
					_, err = io.Copy(out, resp.Body)

					putObject(block.Processed.Transaction.Payload.Contract)
					sqlStatement = `UPDATE contracts SET token_logo = $2 WHERE name = $1 RETURNING id`
					contractID := 0
					err = db.QueryRow(sqlStatement, block.Processed.Transaction.Payload.Contract, kwargsKV.Value).Scan(&contractID)
					if err != nil {
						panic(err)
					}
				}

			} else if kwargsKV.Key == "token_name" {
				sqlStatement = `UPDATE contracts SET token_name = $2 WHERE name = $1 RETURNING id`
				contractID := 0
				err = db.QueryRow(sqlStatement, block.Processed.Transaction.Payload.Contract, kwargsKV.Value).Scan(&contractID)
				if err != nil {
					panic(err)
				}
				err = redisClient.Do("HMSET", "h_tau_token:"+block.Processed.Transaction.Payload.Contract, "name", kwargsKV.Value).Err()

			} else if kwargsKV.Key == "token_symbol" {
				sqlStatement = `UPDATE contracts SET token_symbol = $2 WHERE name = $1 RETURNING id`
				contractID := 0
				err = db.QueryRow(sqlStatement, block.Processed.Transaction.Payload.Contract, kwargsKV.Value).Scan(&contractID)
				if err != nil {
					panic(err)
				}
				err = redisClient.Do("HMSET", "h_tau_token:"+block.Processed.Transaction.Payload.Contract, "symbol", kwargsKV.Value).Err()
			}
		}

		if err = tx.Commit(); err != nil {
			fmt.Printf("error committing")
		}
	}

	err = redisClient.Set("latest_block_number", block.Number, 0).Err()
	if err != nil {
		fmt.Println(err)
	}
}

func syncBlock(t time.Time) {
	syncRunning = 1
	fmt.Printf("syncing block...\n")
	flag.Parse()
	log.SetFlags(0)

	dbLatestBlockNumber, err := redisClient.Get("latest_block_number").Result()
	if err != nil {
		fmt.Printf("error getting block hash")
	}

	res, err := http.Get("https://arko-mn-1.lamden.io/latest_block")
	if err != nil {
		log.Fatal(err)
	}
	body, err := ioutil.ReadAll(res.Body)
	res.Body.Close()
	if err != nil {
		log.Fatal(err)
	}
	if res.StatusCode != 200 {
		log.Fatal("Unexpected status code", res.StatusCode)
	}
	var latestBlock LatestBlock
	err = json.Unmarshal(body, &latestBlock)
	if err != nil {
		log.Fatal(err)
	}

	fmt.Printf("latest block: %s\n", latestBlock.Number)
	fmt.Printf("db latest block: %s\n", dbLatestBlockNumber)

	if latestBlock.Number != dbLatestBlockNumber {
		var apiURL = fmt.Sprintf("https://arko-bs-1.lamden.io/blocks?start_block=%s&limit=50", dbLatestBlockNumber)
		fmt.Printf("\n\n\napiurl: %s\n", apiURL)
		res, err = http.Get(apiURL)
		if err != nil {
			log.Fatal(err)
		}
		body, err = ioutil.ReadAll(res.Body)
		res.Body.Close()
		if err != nil {
			log.Fatal(err)
		}
		if res.StatusCode != 200 {
			log.Fatal("Unexpected status code", res.StatusCode)
		}

		var blocks []Block
		err = json.Unmarshal(body, &blocks)
		if err != nil {
			log.Fatal(err)
		}

		fmt.Printf("blocks len: %d\n", len(blocks))

		for i, block := range blocks {
			// Access and work with each 'block' element here
			fmt.Printf("Looping block index %d number %s timestamp: %s\n", i, block.Number, block.HLCTimestamp)

			sqlStatement := `SELECT id FROM blocks WHERE hash = $1`
			rows := db.QueryRow(sqlStatement, block.Hash)
			var blockHashFound int
			err = rows.Scan(&blockHashFound)
			if err != nil {
				blockHashFound = 0
			}

			if blockHashFound == 0 {
				processBlock(block)
			}

		}

	}
	syncRunning = 0
}

func doEvery(d time.Duration, f func(time.Time)) {
	for x := range time.Tick(d) {
		fmt.Printf("\n\n\nCalling syncBlock function")
		if syncRunning == 0 {
			f(x)
		}
	}
}

func main() {

	//func syncBlock(t time.Time) {
	//GOOS=linux GOARCH=amd64 go build -v block.go
	/*
		TRUNCATE table addresses;
		TRUNCATE table blocks;
		TRUNCATE table dapps;
		TRUNCATE table duplicate_transactions;
		TRUNCATE table top_addresses;
		TRUNCATE table transactions;
		ALTER SEQUENCE addresses_id_seq RESTART WITH 1;
		ALTER SEQUENCE blocks_id_seq RESTART WITH 1;
		ALTER SEQUENCE dapps_id_seq RESTART WITH 1;
		ALTER SEQUENCE top_addresses_id_seq RESTART WITH 1;
		ALTER SEQUENCE top_contracts_id_seq RESTART WITH 1;
		ALTER SEQUENCE transactions_id_seq RESTART WITH 1;



		TRUNCATE table contracts;
		ALTER SEQUENCE contracts_id_seq RESTART WITH 1;
	*/

	fmt.Printf("line main()\n")

	//err := godotenv.Load(".env")
	err := godotenv.Load("/app/config/.env")
	if err != nil {
		log.Fatalf("Error loading .env file")
	}

	redisClient = redis.NewClient(&redis.Options{
		Addr:     os.Getenv("REDISHOST"),
		Password: os.Getenv("REDISPASS"),
		DB:       0,
	})

	psqlInfo := fmt.Sprintf("host=%s port=%d user=%s "+
		"password=%s dbname=%s sslmode=disable",
		os.Getenv("DBHOST"), 5000, os.Getenv("DBUSER"), os.Getenv("DBPASS"), os.Getenv("DBNAME"))
	db, err = sql.Open("postgres", psqlInfo)
	if err != nil {
		panic(err)
	}
	defer db.Close()

	//syncBlock()
	doEvery(3*time.Second, syncBlock)

}
