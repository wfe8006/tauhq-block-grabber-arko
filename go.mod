module tauhq.com/tauhq-block-grabber

go 1.15

require (
	github.com/aws/aws-sdk-go v1.44.14
	github.com/getsentry/sentry-go v0.13.0
	github.com/go-redis/redis v6.15.9+incompatible
	github.com/gorilla/websocket v1.5.0
	github.com/joho/godotenv v1.4.0
	github.com/lib/pq v1.10.5
	golang.org/x/net v0.0.0-20220909164309-bea034e7d591 // indirect
)
